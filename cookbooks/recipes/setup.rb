node.set['apache']['version'] = '2.4'

include_recipe "phpapp::setup_shared"


app = search(:aws_opsworks_app).first
instance = search(:aws_opsworks_instance).first


# set any php.ini settings needed
template "/etc/php.d/app.ini" do
  source "php.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

# set apache2 hosts
web_app "#{app['name']}" do
  server_name "#{app['shortname']}"
  docroot "/srv/#{app['shortname']}/current/"
  template "webapp.conf.erb"
end

execute "touch /etc/httpd/#{app['shortname']}-envvars.conf" do
    ignore_failure true
end

execute "sed -i -e 's/denied/granted/g' /etc/httpd/conf/httpd.conf" do
    ignore_failure true
end


execute "touch /etc/logrotate.d/#{app['shortname']}-log-rotation" do
    ignore_failure true
end

#Script log rotation for apache logs (GDPR)
execute "echo '/var/log/httpd/*.log {
        rotate 1
        missingok
        copytruncate
        sharedscripts

        postrotate
                /bin/rm -rf /var/log/httpd/*.log.*
        endscript
}' >> /etc/logrotate.d/#{app['name']}-log-rotation" do
    ignore_failure true
end

#CloudWatch
execute "yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64" do

end
execute "cd /opt && wget https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip && unzip CloudWatchMonitoringScripts-1.2.2.zip && rm -rf CloudWatchMonitoringScripts-1.2.2.zip" do
    ignore_failure true
end


app['environment'].each do |key, value|
  execute "echo -e '#{key}=#{value}' >> /var/spool/cron/root" do

	end
end

# run CloudWatch cron
cron 'cloudwatch' do
  hour '*'
  minute '*/5'
  command "/opt/aws-scripts-mon/mon-put-instance-data.pl --mem-util --disk-space-util --disk-path=/ --from-cron"
end

#Script log rotation for apache logs (GDPR)
execute "echo '/srv/#{app['shortname']}/current/app/log/*.log {
        rotate 1
        missingok
        copytruncate
        sharedscripts

        postrotate
                /bin/rm -rf /srv/#{app['shortname']}/current/app/log/*.log.*
        endscript
}' >> /etc/logrotate.d/#{app['name']}-app-log-rotation" do
    ignore_failure true
end

cron 'Apache hourly' do
  hour '1'
  minute '0'
  command "/usr/sbin/logrotate -f /etc/logrotate.d/#{app['name']}-log-rotation"
end

cron 'Application hourly' do
  hour '1'
  minute '0'
  command "/usr/sbin/logrotate -f /etc/logrotate.d/#{app['name']}-app-log-rotation"
end

