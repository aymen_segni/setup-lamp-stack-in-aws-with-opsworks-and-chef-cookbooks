#ruby 2.3.1 
#author : Aymen Segni

#installing composer with the current php version
execute "curl -sS https://getcomposer.org/installer | php" do
    ignore_failure true
end

#execute "composer global require 'laravel/lumen-installer'" do
#    ignore_failure true
#end

execute "mv composer.phar /usr/local/bin/composer" do
    ignore_failure true
end

execute "ln -s /usr/local/bin/composer /usr/bin/composer" do
    ignore_failure true
end



#adding public keys of bitbucket to known_hosts
execute "echo '#bitbucket_public_key'" do
    ignore_failure true
end

#creating, filling and changing rights on the private key
execute "touch /root/.ssh/bitbucket.key" do
    ignore_failure true
end

execute "touch /root/.ssh/config" do
    ignore_failure true
end

#default ssh_key usage for bitbucket
execute "echo 'host  bitbucket.org
HostName bitbucket.org
IdentityFile ~/.ssh/bitbucket.key
User git' >> /root/.ssh/config" do
	ignore_failure
end


execute "chmod 600 /root/.ssh/bitbucket.key" do
    ignore_failure true
end

#default Ssh_Private_key for bitbucket (Deploy)
execute "echo '#private_bitbucket_deployment_key' >> /root/.ssh/bitbucket.key" do
    ignore_failure true
end