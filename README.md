# Automate the setup and depolyment of high available, high scalable PHP app with LAMP stack in AWS:

## 1- Description:

Automate the setup and the deplyment of Linux-Apache2-MySQL-PHP stack in AWS using Opsworks,
CloudFormation, Chef Cookbooks, EC2, ELB and S3 

**Architecture of App depolyment in AWS**


![Stack Design](Stack_design.jpg)

## 2- Introduction:


### 2.1- What's AWS Opsworks
**1.  AWS Opsworks**

AWS OpsWorks Stacks lets you manage applications and servers on AWS and on-premises. With OpsWorks Stacks, you can model your application as a stack containing different layers, such as load balancing, database, and application server. You can deploy and configure Amazon EC2 instances in each layer or connect other resources such as Amazon RDS databases. OpsWorks Stacks lets you set automatic scaling for your servers based on preset schedules or in response to changing traffic levels, and it uses lifecycle hooks to orchestrate changes as your environment scales. You run Chef recipes using Chef Solo, allowing you to automate tasks such as installing packages and programming languages or frameworks, configuring software, and more.
OpsWorks has three offerings, AWS Opsworks for Chef Automate, AWS OpsWorks for Puppet Enterprise, and AWS OpsWorks Stacks.
In this project, I use the standard Opsworks offer: *AWS OpsWorks Stacks***** 





### 2.2- Basic Apps server in Opsworks Stack architecture

AWS OpsWorks Stacks provides a simple and flexible way to create and manage stacks and applications.

Here's how a basic application server stack might look with AWS OpsWorks Stacks. It consists of a group of application servers running behind an Elastic Load Balancing load balancer, with a backend Amazon RDS database server.

 ![Basic app in opsworks](php_walkthrough_arch_4.png)
 
### 2.3- Chef Cookbooks and Recipes


AWS OpsWorks Stacks uses Chef cookbooks to handle tasks such as installing php, httpd,.. and configuring packages and deploying apps.

Your custom cookbooks must be stored in an online repository, either an archive such as a .zip file or a source control manager such as Git. When you install or update the cookbooks, AWS OpsWorks Stacks installs the entire repository in a local cache on each of the stack's instances. When an instance needs, for example, to run one or more recipes, it uses the code from the local cache.

Each cookbook directory has least one and typically all of the following standard directories and files, which must use standard names:

* attributes – The cookbook's attributes files.

* recipes – The cookbook's recipe files.

* templates – The cookbook's template files.

* other – Optional user-defined directories that contain other file types, such as definitions or specs.

* metadata.rb – The cookbook's metadata.



## 3- upload chef cookbooks to S3 and create the OpsWorks Stack


### 3.1-  Store cookbook repositories
### 3.2-  Create a stack, layer, and an instance in AWS OpsWorks Stacks with CloudFormation

https://gitlab.com/aymen_segni/setup-lamp-stack-in-aws-with-opsworks-and-chef-cookbooks/wikis/How-to-setup:-upload-chef-cookbooks-to-S3-and-create-the-OpsWorks-Stack

