app = search(:aws_opsworks_app).first
deploy_to = "/srv/#{app['shortname']}"

app_source = app['app_source']

      deploy deploy_to do
      provider Chef::Provider::Deploy::Timestamped
	  keep_releases 2
      repository app_source['url']
      revision app_source['revision']
      migrate false
	  purge_before_symlink(['log', 'tmp/pids', 'public/system'])
      create_dirs_before_symlink(['tmp', 'public', 'config'])
      symlink_before_migrate({})
      symlinks({"system" => "public/system", "pids" => "tmp/pids", "log" => "log"})
      action :deploy

end


execute "echo '' > /etc/httpd/#{app['shortname']}-envvars.conf" do
	ignore_failure true
end


app['environment'].each do |key, value|

	execute "echo -e 'SetEnv #{key} #{value}' >> /etc/httpd/#{app['shortname']}-envvars.conf" do
		ignore_failure true
	end

 end

 app['environment'].each do |key, value|

 	execute "echo -e 'export #{key}=#{value}' >> /etc/bashrc" do
 	end
 end

execute "cd /srv/#{app['shortname']}/current/ && composer install" do
    ignore_failure true
end

execute "cd /srv/#{app['shortname']}/current/ &&  chown -R apache:apache app/ " do
    ignore_failure true
end

execute "mkdir /srv/#{app['shortname']}/shared/log" do
  ignore_failure true
end

execute "service httpd restart" do
    ignore_failure true
end
